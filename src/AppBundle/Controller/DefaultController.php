<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        //$user = $event->getAuthenticationToken()->getUser();
        //$language = $user->getLocale();
        /*$locale = $request->getLocale();
        //$user = $event->getAuthenticationToken()->getUser();
        var_dump($locale);*/
        
        $user = $this->container->get('security.context')->getToken()->getUser();
        //$user = $this->container->get('security.context')->getToken()->getLocale();
        
        return $this->render('default/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir') . '/..'),
        ));
        /*return $this->render('default/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ));*/
    }
    
    /**
     * @Route("/app/", name="dashboard")
     */    
    public function dashboardAction(Request $request)
    {
        return $this->render('app/dashboard/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir') . '/..'),
        ));
        /*return $this->render('default/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ));*/
    }
    
    /**
     * @Route("/app2/", name="dashboard2")
     */    
    public function dashboarddAction(Request $request)
    {
        return $this->render('app/dashboardfuncional/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir') . '/..'),
        ));
        /*return $this->render('default/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ));*/
    }
}