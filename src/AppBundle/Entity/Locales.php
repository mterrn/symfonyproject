<?php
// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="locales")
 */
class Locales
{
    /**
     * @var integer
     *
     * @ORM\Column(name="locale_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $localeId;

    /**
     * @var string
     *
     * @ORM\Column(name="locale_name", type="string", length=45, nullable=false)
     */
     protected $localeName;



    /**
     * Get localeId
     *
     * @return integer 
     */
    public function getLocaleId()
    {
        return $this->localeId;
    }

    /**
     * Set localeName
     *
     * @param string $localeName
     * @return Locales
     */
    public function setLocaleName($localeName)
    {
        $this->localeName = $localeName;

        return $this;
    }

    /**
     * Get localeName
     *
     * @return string 
     */
    public function getLocaleName()
    {
        return $this->localeName;
    }
}